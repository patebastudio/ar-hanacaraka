using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Add on for Mainkan")]
    public int correctButtonIndex;
    public int correctAksaraIndex;
    public int wrongAksaraIndex;
    public TextMeshProUGUI randomizeNextStepText;
    public GameObject questionPanel;
    public GameObject correctAnswerPanel;
    public GameObject wrongAnswerPanel;
    public List<Button> answerButtons;
    public List<string> aksara;

    public void GenerateQuestion(int index)
    {
        questionPanel.SetActive(true);
        correctAnswerPanel.SetActive(false);
        wrongAnswerPanel.SetActive(false);

        foreach (Button btn in answerButtons)
            btn.onClick.RemoveAllListeners();

        correctAksaraIndex = wrongAksaraIndex = index;
        while (wrongAksaraIndex == correctAksaraIndex)
            wrongAksaraIndex = Random.Range(0, aksara.Count - 1);

        correctButtonIndex = Random.Range(0, 5);
        if (correctButtonIndex == 0 ||
            correctButtonIndex == 2 ||
            correctButtonIndex == 4)
        {
            answerButtons[0].GetComponentInChildren<TextMeshProUGUI>().text = aksara[correctAksaraIndex];
            answerButtons[0].onClick.AddListener(() => GameOverPanel(true));

            answerButtons[1].GetComponentInChildren<TextMeshProUGUI>().text = aksara[wrongAksaraIndex];
            answerButtons[1].onClick.AddListener(() => GameOverPanel(false));
        }
        else
        {
            answerButtons[0].GetComponentInChildren<TextMeshProUGUI>().text = aksara[wrongAksaraIndex];
            answerButtons[0].onClick.AddListener(() => GameOverPanel(false));

            answerButtons[1].GetComponentInChildren<TextMeshProUGUI>().text = aksara[correctAksaraIndex];
            answerButtons[1].onClick.AddListener(() => GameOverPanel(true));
        }
    }

    public void GameOverPanel(bool cond)
    {
        questionPanel.SetActive(false);
        if (cond)
        {
            correctAnswerPanel.SetActive(true);
            randomizeNextStepText.text = $"{Random.Range(1, 10)}";
        }
        else
        {
            wrongAnswerPanel.SetActive(true);
        }
    }

    public void ResetQuestion()
    {
        correctAksaraIndex = wrongAksaraIndex = 0;
        questionPanel.SetActive(false);
        correctAnswerPanel.SetActive(false);
        wrongAnswerPanel.SetActive(false);
    }

    public void ExitApp()
    {
        Application.Quit();
    }
}
